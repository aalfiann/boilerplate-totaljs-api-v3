# Boilerplate TotalJS API v3
Boilerplate TotalJS API v3.

[![js-semistandard-style](https://raw.githubusercontent.com/standard/semistandard/master/badge.svg)](https://github.com/standard/semistandard)

## Minimum Requirement
- TotalJS v3
- NodeJS v10
- PostgreSQL

## How to use
- Download, Extract and run `npm install`
- Edit `config`
- Run `node index.js`

## Code of Conducts
1. Programming code style is using [Semi Standard](https://github.com/standard/semistandard).
2. All js files must using **'use strict';**.
3. Response output for internal server must using **definitions/handler.js**.
4. Must provide **postman.js file** so each developer would be easy to make a test.
5. Every global configuration must be set into **config** file at root project (no hardcode).
6. Please provide an unit test file for any standalone classes (outside framework) library at **test** directory.
7. Every function must be documented by comment.
8. Everytime finish doing job, you have to run `npm test`.

## Need to be discuss
1. Standardize the route url.
2. Database structure must be opened to everyone.
3. Well documentation.

## Unit Test
If you want to run unit test
```
npm test
```
