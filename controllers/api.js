/* global CORS */

'use strict';

exports.install = function () {
  // Sets cors for the entire API
  CORS();
};
