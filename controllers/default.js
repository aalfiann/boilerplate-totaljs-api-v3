/* global F ROUTE */

'use strict';

const handler = require(F.path.definitions('handler'));

exports.install = function () {
  ROUTE('/', index, ['get']);

  ROUTE('#401', function () {
    handler.unauthorized(this, 'You are not authorized!');
  });

  ROUTE('#403', function () {
    handler.forbidden(this, 'This page is forbidden!');
  });

  ROUTE('#404', function () {
    handler.notFound(this, 'Page not found!');
  }, ['get', 'post']);

  ROUTE('#408', function () {
    handler.timeout(this, 'Your request is too slow!');
  });

  ROUTE('#409', function () {
    handler.conflict(this, 'Something went wrong!');
  });

  ROUTE('#500', function () {
    handler.custom(this, 500, 'Something went wrong!', 'error', { error: 'Internal Server Error!' });
  });
};

function index () {
  handler.success(this, 'successful render.', { name: 'tester' });
}
